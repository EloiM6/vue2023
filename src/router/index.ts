import type { RouteRecordRaw} from "vue-router";

const routes:Array<RouteRecordRaw> = [
    {
      path:'/',
      name:'Home',
      component:()=> import('@/components/prova1.vue')
    },
    {
        path:'/About',
        name:'About',
        component:()=> import('@/views/About.vue')
    },

];
export default routes;